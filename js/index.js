/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        //getGeolocation();
        //bgGeoLocation();
        /* 
        Just alert 
        */
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

function bgGeoLocation() {

    alert('Geolocation started');
    // Your app must execute AT LEAST ONE call for the current position via standard Cordova geolocation,
    //  in order to prompt the user for Location permission.
    window.navigator.geolocation.getCurrentPosition(function(location) {
        console.log('Location from Phonegap');
        alert('Location from Phonegap');
        //alert('Location from Phonegap');
    });

    var bgGeo = window.plugins.backgroundGeoLocation;

    /**
    * This would be your own callback for Ajax-requests after POSTing background geolocation to your server.
    */
    var yourAjaxCallback = function(response) {
        ////
        // IMPORTANT:  You must execute the #finish method here to inform the native plugin that you're finished,
        //  and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
        // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
        //
        //
        bgGeo.finish();
    };

    /**
    * This callback will be executed every time a geolocation is recorded in the background.
    */
    var callbackFn = function(location) {
        
        //alert('[js] BackgroundGeoLocation callback:  ' + location.latitudue + ',' + location.longitude);
        //console.log('[js] BackgroundGeoLocation callback:  ' + location.latitudue + ',' + location.longitude);
        
        // Fill position to div element
        var element = document.getElementById('geolocation');
        element.innerHTML = 'Latitude: '  + location.latitudue      + '<br />' +
                'Longitude: ' + location.longitude                  + '<br />' +
                '<hr />'      + element.innerHTML;
        // Do your HTTP request here to POST location to your server.
        //
        /*
        $.get( "http://gpsts.thend.cz/mobile/add.php", { pos1: location.longitude, pos2: location.latitudue, user: "amotio" } )
            .done(function( data, status ) {
                alert(data+"\n"+status);
        });
        //
        alert("pos1: "+location.longitude+", pos2: "+location.latitudue);
        */

        yourAjaxCallback.call(this);
    };

    var failureFn = function(error) {
        console.log('BackgroundGeoLocation error');
    }

    // BackgroundGeoLocation is highly configurable.

    bgGeo.configure(callbackFn, failureFn, {
        /*
        url: 'http://only.for.android.com/update_location.json', // <-- only required for Android; ios allows javascript callbacks for your http
        params: {                                               // HTTP POST params sent to your server when persisting locations.
            auth_token: 'user_secret_auth_token',
            foo: 'bar'
        },
        */
        desiredAccuracy: 20,
        stationaryRadius: 20,
        distanceFilter: 30,
        debug: true // <-- enable this hear sounds for background-geolocation life-cycle.
    });

    // Turn ON the background-geolocation system.  The user will be tracked whenever they suspend the app.
    bgGeo.start();

    // If you wish to turn OFF background-tracking, call the #stop method.
    // bgGeo.stop()
};

$(document).ready(function(){
    $('#login-button').click(function() {
        
        var username = $( "#username" ).val();
        var password = $( "#password" ).val();
        
        alert('Push\n'+username+"\n"+password);
        
        $.get( "http://gpsts.thend.cz/mobile/login.php?username="+username+"&password="+password )
            .done(function( data, status ) {
                if (data == 1) {
                    //alert( "Přihlášen\nData: "+data );
                    $( "#login-form" ).fadeOut( "slow" );
                    $(".login-error").fadeOut( "slow" );
                } else {
                    //alert( "Špatné heslo" );
                    $(".login-error").fadeIn( "fast" , function() { $(".login-error").delay(2000).fadeOut( "slow" ); });
                };
        });
        return false;
    });
});